<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//awal
Route::get('/', function () {
    return view('index');
});

//login - logout
Route::get('/login', 'AuthController@login')->name('login');
Route::post('/postlogin', 'AuthController@postlogin')->name('postlogin');
Route::get('/logout','AuthController@logout')->name('logout');

//route admin
Route::group(['middleware'=>['auth','checkRole:admin']], function(){
    Route::get('/dashboarda','DashboardController@index')->name('dashboarda');
    Route::get('/dashboard/user', 'OpController@index')->name('operator');
    Route::get('/dashboard/user/pegawai', 'PegawaiController@index')->name('pegawai');
    Route::get('/dashboard/inventory', 'BarangController@index')->name('inventory');
    Route::get('/dashboard/jenis', 'JenisController@index')->name('jenis');
    Route::get('/dashboard/lokasi', 'LokasiController@index')->name('lokasi');
    Route::get('/dashboard/loan', 'PeminjamanController@index')->name('loan');
    Route::get('/dashboard/return', 'DashboardController@return')->name('return');
    Route::get('/dashboard/setting', 'DashboardController@setting')->name('setting');
    //pegawai
    Route::get('/pegawai/add', 'PegawaiController@insert')->name('addpegawai');
    Route::post('/pegawai/create', 'PegawaiController@create')->name('createpegawai');
    Route::get('/pegawai/hapus/{id}', 'PegawaiController@delete')->name('hapuspegawai');
    Route::get('/pegawai/detail/{id}', 'PegawaiController@show')->name('detailpegawai');
    Route::get('/pegawai/cetak_pdf', 'PegawaiController@cetak_pdf')->name('cetakpegawai');
    Route::get('/user/pegawai/json','PegawaiController@json')->name('jsonpegawai');
    Route::get('/pegawai/{id}/edit', 'PegawaiController@edit')->name('editpegawai');
    Route::post('/pegawai/{id}/update', 'PegawaiController@update')->name('updatepegawai');
    //lokasi
    Route::get('/lokasi/add', 'LokasiController@insert')->name('addlokasi');
    Route::post('/lokasi/create', 'LokasiController@create')->name('createlokasi');
    Route::get('/lokasi/hapus/{id}', 'LokasiController@delete')->name('hapuslokasi');
    Route::get('/lokasi/{id}/edit', 'LokasiController@edit')->name('editlokasi');
    Route::post('/lokasi/{id}/update', 'LokasiController@update')->name('updatelokasi');
    Route::get('/lokasi/cetak_pdf', 'LokasiController@cetak_pdf')->name('cetaklokasi');
    Route::get('/lokasi/detail/{id}', 'LokasiController@show')->name('detailokasi');
    //jenis
    Route::get('/jenis/add', 'JenisController@insert')->name('addjenis');
    Route::post('/jenis/create', 'JenisController@create')->name('createjenis');
    Route::get('/jenis/hapus/{id}', 'JenisController@delete')->name('hapusjenis');
    Route::get('/jenis/{id}/edit', 'JenisController@edit')->name('editjenis');
    Route::get('/jenis/detail/{id}', 'JenisController@show')->name('detailjenis');
    Route::post('/jenis/{id}/update', 'JenisController@update')->name('updatejenis');
    Route::get('/jenis/cetak_pdf', 'JenisController@cetak_pdf')->name('cetakjenis');
    //operator
    Route::get('/operator/add', 'OpController@insert')->name('addoperator');
    Route::post('/operator/create', 'OpController@create')->name('createoperator');
    Route::get('/operator/hapus/{id}', 'OpController@delete')->name('hapusoperator');
    Route::get('/operator/detail/{id}', 'OpController@show')->name('detailoperator');
    Route::get('/op/{id}/edit', 'OpController@edit')->name('editop');
    Route::post('/op/{id}/update', 'OpController@update')->name('updateop');
    //inventory
    Route::get('/inventory/add', 'BarangController@insert')->name('addbarang');
    Route::post('/inventory/create', 'BarangController@create')->name('createbarang');
    Route::get('/inventory/hapus/{id}', 'BarangController@delete')->name('hapusbarang');
    Route::get('/inventory/{id}/edit', 'BarangController@edit')->name('editbarang');
    Route::post('/inventory/{id}/update', 'BarangController@update')->name('updatebarang');
    Route::get('/inventory/cetak_pdf', 'BarangController@cetak_pdf')->name('cetakbarang');
    Route::get('/inventory/detail/{id}', 'BarangController@show')->name('detailbarang');
    //loan
    Route::get('/loan/cetak_pdf', 'PeminjamanController@cetak_pdf')->name('cetakloan');
    Route::get('/loan/detail/{id}', 'PeminjamanController@show')->name('detailoan');
    Route::get('/loan/{id}/edit', 'PeminjamanController@edit')->name('editloan');
    Route::post('/loan/{id}/update', 'PeminjamanController@update')->name('updateloan');
    //return
    Route::get('/return/cetak_pdf', 'PengembalianController@cetak_pdf')->name('cetakreturn');
    Route::get('/return/detail/{id}', 'PengembalianController@show')->name('detailreturn');
});

Route::group(['middleware'=>['auth','checkRole:op']], function(){
    Route::get('/dashboard/op','DashboardController@op')->name('dashboardop');
    Route::get('/op/inventory', 'OperatorB@index')->name('barangop');
    Route::get('/op/setting', 'OperatorB@setting')->name('settingop');
    Route::get('/op/detail/{id}', 'OperatorB@show')->name('detailop');
    Route::get('/op/loan', 'Pinjam_Op@index')->name('loanop');
    Route::get('/op/loan/detail', 'Pinjam_Op@show')->name('dloanop');
    Route::get('/loanop/{id}/edit', 'OpeController@edit')->name('editloanop');
    Route::post('/loanop/{id}/update', 'OpeController@update')->name('updateloanop');
    Route::get('/op/return', 'OpeController@return')->name('returnop');
    Route::get('/op/add', 'Pegawai_Op@insert')->name('addop');
    Route::post('/op/pegawai/create', 'Pegawai_Op@create')->name('createop');
    Route::get('/op/pegawai/hapus/{id}', 'Pegawai_Op@delete')->name('hapuspgw');
    Route::get('/op/pegawai/detail/{id}', 'Pegawai_Op@show')->name('detailpgw');
    Route::get('/op/pegawai/cetak_pdf', 'Pegawai_Op@cetak_pdf')->name('cetakpgw');
    Route::get('/op/user/pegawai/json','Pegawai_Op@json')->name('jsonpgw');
    Route::get('/op/user/pegawai', 'Pegawai_Op@index')->name('pegawaiop');
    Route::get('/detail/{id}', 'OpBarang@show')->name('detailbrgop'); 
    Route::get('/return/cetak_pdf', 'PengembalianOpController@cetak_pdf')->name('cetakreturn');
    Route::get('/returnop/detail/{id}', 'PengembalianOpController@show')->name('detailreturn');
    Route::post('/createkbl', 'KblOpController@create')->name('pengembalian');
    Route::get('/op/kbladd', 'KblOpController@insert')->name('addkbl');
    Route::get('/op/addkembali', 'PengembalianOpController@insert')->name('addkembaliop');
});

Route::group(['middleware'=>['auth','checkRole:pegawai']], function(){
    Route::get('/dashboardb','DashboardController@pegawai')->name('dashboardpgw');
    Route::get('/inventory', 'PegawaiB@index')->name('barangpgw');
    Route::get('/setting', 'PegawaiB@setting')->name('settingpgw');
    Route::get('/detail/{id}', 'PegawaiB@show')->name('detailbarangp');
    Route::get('/inventory/pinjam', 'PegawaiB@insert')->name('pinjam');
    Route::post('/create', 'PegawaiB@create')->name('peminjaman');
});

