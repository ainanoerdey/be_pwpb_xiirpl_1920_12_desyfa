-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 21, 2019 at 06:14 AM
-- Server version: 10.1.35-MariaDB
-- PHP Version: 7.2.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `1920_xiirpl_3_12_desyfa`
--

DELIMITER $$
--
-- Procedures
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `add_inventaris` (IN `id_inventaris` VARCHAR(30), IN `jenis` VARCHAR(30), IN `id_ruang` VARCHAR(30), IN `id_petugas` VARCHAR(30), IN `nama` VARCHAR(30), IN `kondisi` VARCHAR(50), IN `keterangan` TEXT, IN `jumlah` INT(5), `tanggal_register` DATE, IN `kode_inventaris` INT(5))  BEGIN
INSERT INTO inventaris (id_inventaris, jenis, id_ruang, id_petugas, id_nama, kondisi, keterangan, jumlah, tanggal_register, kode_inventaris)
VALUES ( 'i003','jns003','p003','Sound','Layak Pakai','Barang Baru',60,'','3');
END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `detail_pinjam`
--

CREATE TABLE `detail_pinjam` (
  `id_peminjaman` varchar(30) NOT NULL,
  `id_inventaris` varchar(30) NOT NULL,
  `jumlah` int(5) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `detail_pinjam`
--

INSERT INTO `detail_pinjam` (`id_peminjaman`, `id_inventaris`, `jumlah`) VALUES
('p001', 'i001', 30);

--
-- Triggers `detail_pinjam`
--
DELIMITER $$
CREATE TRIGGER `updateStokEdit` AFTER UPDATE ON `detail_pinjam` FOR EACH ROW BEGIN

UPDATE inventaris
SET jumlah = jumlah + (NEW.jumlah - OLD.jumlah)
WHERE id_inventaris = NEW.id_inventaris;

END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `inventaris`
--

CREATE TABLE `inventaris` (
  `id_inventaris` varchar(30) NOT NULL,
  `id_jenis` varchar(30) NOT NULL,
  `id_ruang` varchar(30) NOT NULL,
  `id_petugas` varchar(30) NOT NULL,
  `nama` varchar(30) NOT NULL,
  `kondisi` varchar(50) DEFAULT NULL,
  `keterangan` text,
  `jumlah` int(5) DEFAULT NULL,
  `tanggal_register` date DEFAULT NULL,
  `kode_inventaris` int(5) DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `inventaris`
--

INSERT INTO `inventaris` (`id_inventaris`, `id_jenis`, `id_ruang`, `id_petugas`, `nama`, `kondisi`, `keterangan`, `jumlah`, `tanggal_register`, `kode_inventaris`, `updated_at`, `created_at`) VALUES
('i001', 'jns001', 'r003', 'p001', 'Microphone AX5', 'Layak Pakai', 'Barang baru', 50, '0000-00-00', 1, '2019-09-20 02:16:50', '0000-00-00 00:00:00'),
('i003', 'jns003', 'r003', 'p002', 'Keyboard', 'Layak Pakai', 'Barang baru', 20, '0000-00-00', 2, '2019-09-20 02:16:50', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `jenis`
--

CREATE TABLE `jenis` (
  `id_jenis` varchar(30) NOT NULL,
  `nama_jenis` varchar(30) NOT NULL,
  `kode_jenis` int(5) NOT NULL,
  `keterangan` text,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jenis`
--

INSERT INTO `jenis` (`id_jenis`, `nama_jenis`, `kode_jenis`, `keterangan`, `updated_at`, `created_at`) VALUES
('jns001', 'Alat Listrik', 1, 'Nyetrum dah pokonya', '2019-09-19 18:23:51', '0000-00-00 00:00:00'),
('jns002', 'Fasilitas Olahraga', 2, '', '2019-09-17 09:03:30', '0000-00-00 00:00:00'),
('jns003', 'Barang Laboratorium', 3, '', '2019-09-17 09:03:30', '0000-00-00 00:00:00'),
('jns005', 'Alat Las', 4, NULL, '2019-09-19 00:38:36', '2019-09-19 00:38:36');

-- --------------------------------------------------------

--
-- Table structure for table `level`
--

CREATE TABLE `level` (
  `id_level` varchar(30) NOT NULL,
  `nama_level` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `level`
--

INSERT INTO `level` (`id_level`, `nama_level`) VALUES
('level01', 'Administrator'),
('level02', 'Operator');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2019_08_31_031317_create_detail_pinjam_table', 0),
(2, '2019_08_31_031317_create_inventaris_table', 0),
(3, '2019_08_31_031317_create_jenis_table', 0),
(4, '2019_08_31_031317_create_level_table', 0),
(5, '2019_08_31_031317_create_pegawai_table', 0),
(6, '2019_08_31_031317_create_peminjaman_table', 0),
(7, '2019_08_31_031317_create_petugas_table', 0),
(8, '2019_08_31_031317_create_ruang_table', 0),
(9, '2019_08_31_031318_add_foreign_keys_to_detail_pinjam_table', 0),
(10, '2019_08_31_031318_add_foreign_keys_to_inventaris_table', 0),
(11, '2019_08_31_031318_add_foreign_keys_to_peminjaman_table', 0),
(12, '2019_08_31_031318_add_foreign_keys_to_petugas_table', 0),
(13, '2014_10_12_000000_create_users_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `pegawai`
--

CREATE TABLE `pegawai` (
  `id_pegawai` varchar(30) NOT NULL,
  `user_id` int(11) NOT NULL,
  `nama` varchar(30) NOT NULL,
  `nip` varchar(30) NOT NULL,
  `alamat` text,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pegawai`
--

INSERT INTO `pegawai` (`id_pegawai`, `user_id`, `nama`, `nip`, `alamat`, `updated_at`, `created_at`) VALUES
('pg002', 5, 'Gamaliel Audirga', '199408142001031002', 'Jalan Bungur Raya', '2019-09-17 23:51:39', '2019-09-17 23:51:39');

-- --------------------------------------------------------

--
-- Table structure for table `peminjaman`
--

CREATE TABLE `peminjaman` (
  `id_peminjaman` varchar(30) DEFAULT NULL,
  `tanggal_pinjam` date DEFAULT NULL,
  `tanggal_kembali` date DEFAULT NULL,
  `status_peminjaman` varchar(20) NOT NULL,
  `id_pegawai` varchar(30) NOT NULL,
  `id_detail_pinjam` varchar(30) NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `petugas`
--

CREATE TABLE `petugas` (
  `id_petugas` varchar(30) NOT NULL,
  `nama` varchar(30) NOT NULL,
  `user_id` int(11) NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `petugas`
--

INSERT INTO `petugas` (`id_petugas`, `nama`, `user_id`, `updated_at`, `created_at`) VALUES
('p001', 'Mahena', 0, '2019-09-19 03:48:13', '0000-00-00 00:00:00'),
('p002', 'Mahera', 0, '2019-09-19 03:48:13', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `pgw`
--

CREATE TABLE `pgw` (
  `id_pgw` int(11) NOT NULL,
  `nama_pgw` varchar(30) NOT NULL,
  `nip` varchar(30) NOT NULL,
  `alamat` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ruang`
--

CREATE TABLE `ruang` (
  `id_ruang` varchar(30) NOT NULL,
  `nama_ruang` varchar(30) DEFAULT NULL,
  `kode_ruang` varchar(30) DEFAULT NULL,
  `keterangan` text,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ruang`
--

INSERT INTO `ruang` (`id_ruang`, `nama_ruang`, `kode_ruang`, `keterangan`, `updated_at`, `created_at`) VALUES
('r001', 'Ruang Peralatan Olahraga', '001', '', '2019-09-19 18:22:27', '0000-00-00 00:00:00'),
('r002', 'Ruang Laboratorium', '002', '', '2019-09-17 09:00:54', '0000-00-00 00:00:00'),
('r003', 'Ruang Isolasi', '003', '', '2019-09-17 09:00:54', '0000-00-00 00:00:00'),
('r004', 'Lab MM 1', '004', NULL, '2019-09-18 17:25:09', '2019-09-18 17:25:09'),
('RU001', 'Lab RPL 2', NULL, NULL, '2019-09-19 18:11:54', '2019-09-19 18:11:54');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nama` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role` varchar(45) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `nama`, `password`, `role`, `remember_token`, `created_at`, `updated_at`) VALUES
(3, 'Desyfa', '$2y$10$X9g0YeMC5FaK9XChOdi4i.Fls5/wXg2hx/d2JLJptmsVaheOzxyFC', 'admin', 'qqiM0wndm9b2Z8qmtgopRcjMmXCjF5LcJfn0KgApUvSetoo13YQAOxNmi1NL', '2019-09-17 00:20:36', '2019-09-17 00:20:36'),
(4, 'Zulfa', '$2y$10$yJGN9.2D7OHwX7K0kJOoBe/aUgMHiZ718HihHN/R0GAEXzqV.V8OG', 'op', 'rkbBfebrsAjHQFDdg8M24vCxqIFA5PyWni4FuSRuV4afjuGhGoF1X4AIpzs5', '2019-09-17 22:21:32', '2019-09-17 22:21:32'),
(5, 'Gamaliel Audirga', '$2y$10$MjzPPstz1Q9otqurAtuhA.3aLbrbG/d2RBj4S6VLoOGg0gVqyPP/y', 'pegawai', 'RC5xXfLNCdFyw669Fp3sOJvaBeoveRG1ZkG92jV3mzjM04tcYgGcgh9AX8PJ', '2019-09-17 23:51:39', '2019-09-17 23:51:39'),
(6, 'alvin', '$2y$10$M4gVGtyXsY6anz0YRtA9Cee1KUNA/q7T3bHKwDP.KOPYELsCHxFuO', 'Operator', 'Tu98jzI6gX1zUvMPBrhBPMJHKYe7MIkJ0WVU9c9dU39H6nY0kJcdE4eSVpIb', '2019-09-18 23:30:52', '2019-09-18 23:30:52'),
(8, 'Zachra', '$2y$10$kKXbktkF1QBDz3phhQ8nMeENOn7vieQcmxi5fDble6OadEuhE8SBO', 'Operator', 'jsAest4Zfv37aIV5X4fXA1T5gQYDy0jUreTWQ0pIDaqxw0EjNNFbZaKypb2W', '2019-09-18 23:32:34', '2019-09-18 23:32:34');

-- --------------------------------------------------------

--
-- Stand-in structure for view `vgetrecinventaris`
-- (See below for the actual view)
--
CREATE TABLE `vgetrecinventaris` (
`SUM(jumlah)` decimal(32,0)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `vgetrecordinventaris`
-- (See below for the actual view)
--
CREATE TABLE `vgetrecordinventaris` (
`COUNT(*)` bigint(21)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `vgetrecordpegawai`
-- (See below for the actual view)
--
CREATE TABLE `vgetrecordpegawai` (
`COUNT(*)` bigint(21)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `vgetrecordpeminjaman`
-- (See below for the actual view)
--
CREATE TABLE `vgetrecordpeminjaman` (
`COUNT(*)` bigint(21)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `vgetrecordpetugas`
-- (See below for the actual view)
--
CREATE TABLE `vgetrecordpetugas` (
`COUNT(*)` bigint(21)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `vgetrecordruang`
-- (See below for the actual view)
--
CREATE TABLE `vgetrecordruang` (
`COUNT(*)` bigint(21)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `vgetrgbrg`
-- (See below for the actual view)
--
CREATE TABLE `vgetrgbrg` (
`id_ruang` varchar(30)
,`nama_ruang` varchar(30)
);

-- --------------------------------------------------------

--
-- Structure for view `vgetrecinventaris`
--
DROP TABLE IF EXISTS `vgetrecinventaris`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vgetrecinventaris`  AS  select sum(`inventaris`.`jumlah`) AS `SUM(jumlah)` from `inventaris` ;

-- --------------------------------------------------------

--
-- Structure for view `vgetrecordinventaris`
--
DROP TABLE IF EXISTS `vgetrecordinventaris`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vgetrecordinventaris`  AS  select count(0) AS `COUNT(*)` from `inventaris` ;

-- --------------------------------------------------------

--
-- Structure for view `vgetrecordpegawai`
--
DROP TABLE IF EXISTS `vgetrecordpegawai`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vgetrecordpegawai`  AS  select count(0) AS `COUNT(*)` from `pegawai` ;

-- --------------------------------------------------------

--
-- Structure for view `vgetrecordpeminjaman`
--
DROP TABLE IF EXISTS `vgetrecordpeminjaman`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vgetrecordpeminjaman`  AS  select count(0) AS `COUNT(*)` from `peminjaman` ;

-- --------------------------------------------------------

--
-- Structure for view `vgetrecordpetugas`
--
DROP TABLE IF EXISTS `vgetrecordpetugas`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vgetrecordpetugas`  AS  select count(0) AS `COUNT(*)` from `petugas` ;

-- --------------------------------------------------------

--
-- Structure for view `vgetrecordruang`
--
DROP TABLE IF EXISTS `vgetrecordruang`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vgetrecordruang`  AS  select count(0) AS `COUNT(*)` from `ruang` ;

-- --------------------------------------------------------

--
-- Structure for view `vgetrgbrg`
--
DROP TABLE IF EXISTS `vgetrgbrg`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vgetrgbrg`  AS  select `r`.`id_ruang` AS `id_ruang`,`r`.`nama_ruang` AS `nama_ruang` from `ruang` `r` where `r`.`id_ruang` in (select `inventaris`.`id_ruang` from `inventaris`) ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `detail_pinjam`
--
ALTER TABLE `detail_pinjam`
  ADD PRIMARY KEY (`id_peminjaman`),
  ADD KEY `id_inventaris` (`id_inventaris`);

--
-- Indexes for table `inventaris`
--
ALTER TABLE `inventaris`
  ADD PRIMARY KEY (`id_inventaris`),
  ADD KEY `id_jenis` (`id_jenis`),
  ADD KEY `id_ruang` (`id_ruang`),
  ADD KEY `id_petugas` (`id_petugas`);

--
-- Indexes for table `jenis`
--
ALTER TABLE `jenis`
  ADD PRIMARY KEY (`id_jenis`);

--
-- Indexes for table `level`
--
ALTER TABLE `level`
  ADD PRIMARY KEY (`id_level`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pegawai`
--
ALTER TABLE `pegawai`
  ADD PRIMARY KEY (`id_pegawai`);

--
-- Indexes for table `peminjaman`
--
ALTER TABLE `peminjaman`
  ADD KEY `id_pegawai` (`id_pegawai`),
  ADD KEY `id_detail_pinjam` (`id_detail_pinjam`);

--
-- Indexes for table `petugas`
--
ALTER TABLE `petugas`
  ADD PRIMARY KEY (`id_petugas`);

--
-- Indexes for table `pgw`
--
ALTER TABLE `pgw`
  ADD PRIMARY KEY (`id_pgw`);

--
-- Indexes for table `ruang`
--
ALTER TABLE `ruang`
  ADD PRIMARY KEY (`id_ruang`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `pgw`
--
ALTER TABLE `pgw`
  MODIFY `id_pgw` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `detail_pinjam`
--
ALTER TABLE `detail_pinjam`
  ADD CONSTRAINT `detail_pinjam_ibfk_1` FOREIGN KEY (`id_inventaris`) REFERENCES `inventaris` (`id_inventaris`);

--
-- Constraints for table `inventaris`
--
ALTER TABLE `inventaris`
  ADD CONSTRAINT `inventaris_ibfk_1` FOREIGN KEY (`id_jenis`) REFERENCES `jenis` (`id_jenis`),
  ADD CONSTRAINT `inventaris_ibfk_2` FOREIGN KEY (`id_ruang`) REFERENCES `ruang` (`id_ruang`),
  ADD CONSTRAINT `inventaris_ibfk_3` FOREIGN KEY (`id_petugas`) REFERENCES `petugas` (`id_petugas`);

--
-- Constraints for table `peminjaman`
--
ALTER TABLE `peminjaman`
  ADD CONSTRAINT `peminjaman_ibfk_1` FOREIGN KEY (`id_pegawai`) REFERENCES `pegawai` (`id_pegawai`) ON DELETE CASCADE,
  ADD CONSTRAINT `peminjaman_ibfk_2` FOREIGN KEY (`id_detail_pinjam`) REFERENCES `detail_pinjam` (`id_peminjaman`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
