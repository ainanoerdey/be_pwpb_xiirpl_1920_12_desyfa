<!doctype html>
<html lang="en">
<head>

	<meta charset="utf-8" />
	<link rel="icon" type="image/png" href="assets/img/favicon.ico">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

	<title>invenity.org</title>

	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />
    
    <link href="<?php echo e(asset('/css/bootstrap.min.css')); ?>" rel="stylesheet" />
    <link href="<?php echo e(asset('/css/animate.min.css')); ?>" rel="stylesheet"/>
    <link href="<?php echo e(asset('/css/bootstrap.css?v=1.4.0')); ?>" rel="stylesheet"/>
    <link href="<?php echo e(asset('/css/demo.css')); ?>" rel="stylesheet" />
    <!--     Fonts and icons     -->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300' rel='stylesheet' type='text/css'>
    <link href="<?php echo e(asset('/css/pe-icon-7-stroke.css')); ?>" rel="stylesheet" />

</head>
<body>
<div class="wrapper" style="margin-left:40vh;">
    <div class="sidebar">

    	<div class="sidebar-wrapper">
            <div class="logo">
                <img src="<?php echo e(asset('/img/profile.png')); ?>" style="width: 15vh; height: 15vh; margin-left: 8vh;">
                <h6 class="text-center"><?php echo e(auth()->user()->nama); ?></h6>
            </div>
            <ul class="nav">
                <li class="active">
                    <a href="/dashboard">
                        <i class="pe-7s-graph"></i>
                        <p>Dashboard</p>
                    </a>
                </li>
                <li>
                    <a href="/dashboard">
                        <i class="pe-7s-home"></i>
                        <p>Home</p>
                    </a>
                </li>
                <li>
                    <a href="/dashboard/user">
                        <i class="pe-7s-note2"></i>
                        <p>User Management</p>
                    </a>
                </li>
                <li>
                    <a href="/dashboard/inventory">
                        <i class="pe-7s-news-paper"></i>
                        <p>Inventory Management</p>
                    </a>
                </li>
                <li>
                    <a href="/dashboard/loan">
                        <i class="pe-7s-note"></i>
                        <p>Loan Data</p>
                    </a>
                </li>
                <li>
                    <a href="/dashboard/return">
                        <i class="pe-7s-refresh"></i>
                        <p>Return Data</p>
                    </a>
                </li>
                <li>
                    <a href="/dashboard/setting">
                        <i class="pe-7s-settings"></i>
                        <p>Account Setting</p>
                    </a>
                </li>
				<li class="active-pro">
                    <a href="/logout">
                        <i class="pe-7s-rocket"></i>
                        <p>Log Out</p>
                    </a>
                </li>
            </ul>
    	</div>
    </div>


        <div class="container" style="margin-top:5%;">
            <div class="row">
                 <div class="card col-sm-4" style="width: 20rem; margin-top: 2vh; margin-left:2vh;">
                         <div class="card-body">
                           <h3 class="card-title" style="text-align:center; color: darkgreen">200</h3>
                           <h6 class="card-subtitle mb-2 text-muted" style="text-align:center; margin-bottom: 2vh;">Inventory</h6>
                          
                         </div>
                 </div>
                 <div class="card col-sm-4" style="width: 20rem; margin-top: 2vh; margin-left:2vh;">
                         <div class="card-body">
                           <h3 class="card-title" style="text-align:center; color: red">2</h3>
                           <h6 class="card-subtitle mb-2 text-muted" style="text-align:center; margin-bottom: 2vh;">Operator</h6>
                          
                         </div>
                 </div>
                 <div class="card col-sm-4" style="width: 20rem; margin-top: 2vh; margin-left:2vh;">
                         <div class="card-body">
                           <h3 class="card-title" style="text-align:center; color:rgb(53, 50, 205)">50</h3>
                           <h6 class="card-subtitle mb-2 text-muted" style="text-align:center; margin-bottom: 2vh;">Borrowwer</h6>
                          
                         </div>
                 </div>
         </div>
         
     </div>
                      <div class="container">
                            <h2>Loan Data</h2>           
                            <table class="table table-hover">
                              <thead>
                                <tr>
                                  <th>ID PEMINJAMAN</th>
                                  <th>ID INVENTARIS</th>
                                  <th>TANGGAL PINJAM</th>
                                  <th>TANGGAL KEMBALI</th>
                                  <th>ACTION</th>

                                </tr>
                              </thead>
                              <tbody>
                                <tr>
                                  <td>p001</td>
                                  <td>i001</td>
                                  <td>2019, 10 Agustus</td>
                                  <td>2019, 10 Agustus</td>
                                  <td>
                                    <i class="pe-7s-edit"></i>
                                    <i class="pe-7s-info"></i>
                                    <i class="pe-7s-print"></i>
                                  </td>
                                </tr>
                                <tr>
                                        <td>p001</td>
                                        <td>i001</td>
                                        <td>2019, 10 Agustus</td>
                                        <td>2019, 10 Agustus</td>
                                        <td>
                                          <i class="pe-7s-edit"></i>
                                          <i class="pe-7s-info"></i>
                                          <i class="pe-7s-print"></i>
                                        </td>
                                      </tr>
                                      <tr>
                                            <td>p001</td>
                                            <td>i001</td>
                                            <td>2019, 10 Agustus</td>
                                            <td>2019, 10 Agustus</td>
                                            <td>
                                              <i class="pe-7s-edit"></i>
                                              <i class="pe-7s-info"></i>
                                              <i class="pe-7s-print"></i>
                                            </td>
                                          </tr>
                                          <tr>
                                                <td>p001</td>
                                                <td>i001</td>
                                                <td>2019, 10 Agustus</td>
                                                <td>2019, 10 Agustus</td>
                                                <td>
                                                  <i class="pe-7s-edit"></i>
                                                  <i class="pe-7s-info"></i>
                                                  <i class="pe-7s-print"></i>
                                                </td>
                                              </tr>
                                
                              </tbody>
                            </table>
                          </div>
                         
                
        </form>                  
                        <footer class="footer">

                            <p class="copyright pull-right">
                            &copy; <script>document.write(new Date().getFullYear())</script> <a href="http://www.creative-tim.com">Invenity Tim</a>, made with love for an easy vibes.
                            </p>
                            </div>
                        </footer>
            
</body>

    <!--   Core JS Files   -->
    <script src="../../assets/js/jquery.3.2.1.min.js" type="text/javascript"></script>
	<script src="../../assets/js/bootstrap.min.js" type="text/javascript"></script>

	<!--  Charts Plugin -->
	<script src="../../assets/js/chartist.min.js"></script>

    <!--  Notifications Plugin    -->
    <script src="../../assets/js/bootstrap-notify.js"></script>

    <!-- Light Bootstrap Table Core javascript and methods for Demo purpose -->
	<script src="../../assets/js/light-bootstrap-dashboard.js?v=1.4.0"></script>

	<!-- Light Bootstrap Table DEMO methods, don't include it in your project! -->
	<script src="../../assets/js/demo.js"></script>

	<script type="text/javascript">
    	$(document).ready(function(){

        	demo.initChartist();

        	// $.notify({
         //    	icon: 'pe-7s-hand',
         //    	message: "Welcome to <b>Invenity App</b> - an easy vibes for borrowing the inventory."

         //    }
         ({
                type: 'info',
                timer: 4000
            });

    	});
	</script>

</html>
<?php /**PATH D:\xampp\htdocs\be_pwpb_xiirpl_1920_12_desyfa\be_pwpb_xiirpl_1920_12_desyfa\resources\views//Administrator/LoanData.blade.php ENDPATH**/ ?>