<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
  <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" type="text/css" href="<?php echo e(asset('css/style.css')); ?>">
	<title>invenity.org</title>
</head>
<body>
	<section id="header">
		<div class="container-fluid">
			<nav class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
    <div class="container">
      <a class="navbar-brand js-scroll-trigger" href="#page-top"><img style="width: 30%; height: 10%;" src="img/logo.png"></a>
      <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        Menu
        <i class="fas fa-bars"></i>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="#about" style="color: #325021">Why Invenity?</a>
          </li>
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="#projects" style="color: #325021">About</a>
          </li>
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger">
              <input type="Search" name="Search" style="background-color: transparent; border-radius: 15px; border-color: #325021">
            </a>
          </li>
        </ul>
      </div>
    </div>
  </nav>
		</div>
	</section>
	<section class="main" style="background-image: url(img/cactus.jpg)">
		<div class="container">
			<div class="row">
				<div class="col-sm-8" style="margin-top: 10%;">
					<h1 style="font-family: 'Indie Flower'; color: #325021">Welcome here!</h1>
					<p style=" color: #325021">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.  </p>
					<a href="#loginModal" role="button" class="btn btn-success btn-lg" data-toggle="modal">Get Started</a>

					<div id="loginModal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
              <div class="modal-dialog">
                  <div class="modal-content">
                      <div class="modal-header">
                          <h3><?php echo e(__('Login')); ?></h3>
                          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                      </div>
                      <div class="modal-body">
                          <form class="form" role="form" autocomplete="off" id="formLogin" novalidate="" method="POST" action="<?php echo e(route ('postlogin')); ?>">
                          <?php echo csrf_field(); ?>   
                              <div class="form-group">
                                  <label for="nama"><?php echo e(__('Username')); ?></label>
                                  <input id="nama" type="email" class="form-control <?php if ($errors->has('email')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('email'); ?> is-invalid <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>" name="nama" required autocomplete="nama" autofocus>

                                <?php if ($errors->has('email')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('email'); ?>
                                    <span class="invalid-feedback" role="alert">
                                        <strong><?php echo e($message); ?></strong>
                                    </span>
                                <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>
                                </div>
                              <div class="form-group">
                                  <label for="password"><?php echo e(__('Password')); ?></label>
                                  <input id="password" type="password" class="form-control <?php if ($errors->has('password')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('password'); ?> is-invalid <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>" name="password" required autocomplete="current-password">
  
                                  <?php if ($errors->has('password')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('password'); ?>
                                      <span class="invalid-feedback" role="alert">
                                          <strong><?php echo e($message); ?></strong>
                                      </span>
                                  <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>
                              </div>
                              <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" id="rememberMe">
                                <label class="custom-control-label" for="rememberMe">Remember me</label>
                              </div>
                              <div class="form-group row mb-0">
                              <div class="col-md-8">
                              <p></p>
                                  <button type="submit" class="btn btn-primary">
                                      <?php echo e(__('Login')); ?>

                                  </button>
  
                                  <?php if(Route::has('password.request')): ?>
                                      <a class="btn btn-link" href="<?php echo e(route('password.request')); ?>">
                                          <?php echo e(__('Forgot Your Password?')); ?>

                                      </a>
                                  <?php endif; ?>
                              </div>
                          </div>
                      </form
                      </div>
                  </div>
              </div>
				</div>
			</div>
		</div>
	</section>
	<section id="about" class="about-section" style="  min-height: 50vh;">

    <div class="container">
      <div class="row">
        <div class="col-sm-8" style="margin-top: 5%;">
          <h2 style="font-family: 'Indie Flower';color: #325021">What you can borrow?</h2>
          <p style="text-align: left;color: #325021">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. 
        </div>
        	<div class="col-sm-4" style="margin-top: 5%; margin-left: -1.5%; ">
          		 <img style="margin-left: 50px;" src="assets/img/pict-4.png" class=" img-fluid">
          	</div>
      </div>
      <img src="" class="img-fluid" alt="">
    </div>
  </section>
  <section id="about" class="section2">

    <div class="container">
      <div class="row">
		<div class="col-sm-4" style="margin-top: 5%; ">
      		<img src="img/pict-2.png" class="img-fluid">
      	</div>
        <div class="col-sm-8" style="margin-top: 5%;">
          <h2 style="font-family: 'Indie Flower';color: #325021">Make everything easy</h2>
          <p style="text-align: left;color: #325021">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.  Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.  
        </div>

      </div>
      
    </div>
  </section>
  <section id="about" class="about-section" style="  min-height: 50vh;">

    <div class="container">
      <div class="row">
        <div class="col-sm-8" style="margin-top: 5%;">
          <h2 style="font-family: 'Indie Flower';color: #325021">Everything will be ok</h2>
          <p style="text-align: left;color: #325021">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.  Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. 
        </div>
        <div class="col-sm-4" style="margin-top: 5%; margin-left: -1.5%; ">
          		 <img style="margin-left: 50px;" src="img/pict-3.png" class=" img-fluid">
          	</div>

      </div>
     
    </div>
  </section>
  <section id="about" class="section3">

    <div class="container">
      <div class="row"></div>
      <footer class="footer">
      <p style="text-align:center; color:white; margin-top: 2vh;">&copy;Invenity Tim, 2019</p>
      </div>
      </footer>
      </div>
      </div>
      <div class="row"></div>
     
    </div>
  </section>



<script type="text/javascript">
	function openForm() {
  document.getElementById("myForm").style.display = "block";
}

function closeForm() {
  document.getElementById("myForm").style.display = "none";
}
$("#btnLogin").click(function(event) {

//Fetch form to apply custom Bootstrap validation
var form = $("#formLogin")

if (form[0].checkValidity() === false) {
  event.preventDefault()
  event.stopPropagation()
}

form.addClass('was-validated');
});

</script>

<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>


</body>
</html><?php /**PATH D:\xampp\htdocs\be_pwpb_xiirpl_1920_12_desyfa\resources\views/index.blade.php ENDPATH**/ ?>