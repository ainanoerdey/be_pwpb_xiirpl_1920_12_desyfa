<!DOCTYPE html>
<html>
<head>
	<title>PDF</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
	<style type="text/css">
		table tr td,
		table tr th{
			font-size: 9pt;
		}
	</style>
	<center>
		<p>Data Inventaris</p>
	</center>
	<table class='table table-bordered'>
    <thead>
        <tr>
            <th>ID INVENTARIS</th>
            <th>NAMA</th>
            <th>JUMLAH</th>
            <th>KONDISI</th>
            <th>KODE</th>
         </tr>
    </thead>
        <tbody>
        <?php $__currentLoopData = $barang; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $b): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <tr>
            <td><?php echo e($b->id_inventaris); ?></td>
            <td><?php echo e($b->nama); ?></td>
            <td><?php echo e($b->jumlah); ?></td>
            <td><?php echo e($b->kondisi); ?></td>
            <td><?php echo e($b->kode_inventaris); ?></td>
        </tr>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </tbody>
	</table>

</body>
</html><?php /**PATH D:\xampp\htdocs\be_pwpb_xiirpl_1920_12_desyfa\be_pwpb_xiirpl_1920_12_desyfa\resources\views/Administrator/barangpdf.blade.php ENDPATH**/ ?>