<!doctype html>
<html lang="en">
<head>

	<meta charset="utf-8" />
	<link rel="icon" type="image/png" href="assets/img/favicon.ico">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

	<title>invenity.org</title>

	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />

    <link href="<?php echo e(asset('/css/bootstrap.min.css')); ?>" rel="stylesheet" />
    <link href="<?php echo e(asset('/css/animate.min.css')); ?>" rel="stylesheet"/>
    <link href="<?php echo e(asset('/css/bootstrap.css?v=1.4.0')); ?>" rel="stylesheet"/>
    <link href="<?php echo e(asset('/css/demo.css')); ?>" rel="stylesheet" />
    <!--     Fonts and icons     -->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300' rel='stylesheet' type='text/css'>
    <link href="<?php echo e(asset('/css/pe-icon-7-stroke.css')); ?>" rel="stylesheet" />

</head>
<body>
<div class="wrapper" style="margin-left:40vh;">
    <div class="sidebar">

    	<div class="sidebar-wrapper">
            <div class="logo">
                <img src="<?php echo e(asset('/img/profile.png')); ?>" style="width: 15vh; height: 15vh; margin-left: 8vh;">
                <h6 class="text-center"><?php echo e(auth()->user()->nama); ?></h6>
            </div>
            <ul class="nav">
                <li class="active">
                    <a href="/dashboard">
                        <i class="pe-7s-graph"></i>
                        <p>Dashboard</p>
                    </a>
                </li>
                <li>
                    <a href="/dashboard">
                        <i class="pe-7s-home"></i>
                        <p>Home</p>
                    </a>
                </li>
                <li>
                    <a href="/dashboard/user">
                        <i class="pe-7s-note2"></i>
                        <p>User Management</p>
                    </a>
                </li>
                <li>
                    <a href="/dashboard/inventory">
                        <i class="pe-7s-news-paper"></i>
                        <p>Inventory Management</p>
                    </a>
                </li>
                <li>
                    <a href="/dashboard/loan">
                        <i class="pe-7s-note"></i>
                        <p>Loan Data</p>
                    </a>
                </li>
                <li>
                    <a href="/dashboard/return">
                        <i class="pe-7s-refresh"></i>
                        <p>Return Data</p>
                    </a>
                </li>
                <li>
                    <a href="/dashboard/setting">
                        <i class="pe-7s-settings"></i>
                        <p>Account Setting</p>
                    </a>
                </li>
				<li class="active-pro">
                    <a href="/logout">
                        <i class="pe-7s-rocket"></i>
                        <p>Log Out</p>
                    </a>
                </li>
            </ul>
    	</div>
    </div>


        <div class="container" style="margin-top:5%;">
            <div class="row">
                 <div class="card col-sm-4" style="width: 20rem; margin-top: 2vh; margin-left:2vh;">
                         <div class="card-body">
                           <h3 class="card-title" style="text-align:center; color: darkgreen">200</h3>
                           <h6 class="card-subtitle mb-2 text-muted" style="text-align:center; margin-bottom: 2vh;">Inventory</h6>
                          
                         </div>
                 </div>
                 <div class="card col-sm-4" style="width: 20rem; margin-top: 2vh; margin-left:2vh;">
                         <div class="card-body">
                           <h3 class="card-title" style="text-align:center; color: red">2</h3>
                           <h6 class="card-subtitle mb-2 text-muted" style="text-align:center; margin-bottom: 2vh;">Operator</h6>
                          
                         </div>
                 </div>
                 <div class="card col-sm-4" style="width: 20rem; margin-top: 2vh; margin-left:2vh;">
                         <div class="card-body">
                           <h3 class="card-title" style="text-align:center; color:rgb(53, 50, 205)">50</h3>
                           <h6 class="card-subtitle mb-2 text-muted" style="text-align:center; margin-bottom: 2vh;">Borrowwer</h6>
                          
                         </div>
                 </div>
         </div>
         
     </div>
     <div class="container"> 
                <div class="row">
                <form action="/inventory/<?php echo e($barang->id_inventaris); ?>/update" method="post">
                                <?php echo csrf_field(); ?>
                                <div class="form-row">
                                          <div class="form-group col-md-4">
                                            <label for="id_inventaris">Id Inventaris</label>
                                            <input name="id_inventaris" type="text" class="form-control" id="id_inventaris" value="<?php echo e($barang->id_inventaris); ?>">
                                          </div>
                                </div> 
                                <div class="form-row">
                                          <div class="form-group col-md-4">
                                            <label for="id_jenis">Id Jenis</label>
                                            <input name="id_jenis" type="text" class="form-control" id="id_jenis" value="<?php echo e($barang->id_jenis); ?>">
                                          </div>
                                </div> 
                                <div class="form-row">
                                          <div class="form-group col-md-4">
                                            <label for="id_petugas">Id Petugas</label>
                                            <input name="id_petugas" type="text" class="form-control" id="id_inventaris" value="<?php echo e($barang->id_petugas); ?>"> 
                                          </div>
                                </div> 
                                <div class="form-row">
                                          <div class="form-group col-md-4">
                                            <label for="id_ruang">Id Ruang</label>
                                            <input name="id_ruang" type="text" class="form-control" id="id_ruang" value="<?php echo e($barang->id_ruang); ?>">
                                          </div>
                                </div>        
                                <div class="form-row">
                                          <div class="form-group col-md-4">
                                            <label for="nama">Nama</label>
                                            <input name="nama" type="text" class="form-control" id="nama" value="<?php echo e($barang->nama); ?>">
                                          </div>
                                </div> 
                                <div class="form-row">
                                          <div class="form-group col-md-4">
                                            <label for="kondisi">Kondisi</label>
                                            <input name="kondisi" type="text" class="form-control" id="kondisi" value="<?php echo e($barang->kondisi); ?>"> 
                                          </div>
                                </div> 
                                <div class="form-row">
                                          <div class="form-group col-md-4">
                                            <label for="keterangan">Keterangan</label>
                                            <input name="keterangan" type="text" class="form-control" id="keterangan" value="<?php echo e($barang->keterangan); ?>">
                                          </div>
                                </div> 
                                <div class="form-row">
                                          <div class="form-group col-md-4">
                                            <label for="kode_inventaris">Kode</label>
                                            <input name="kode_inventaris" type="int" class="form-control" id="kode_inventaris" value="<?php echo e($barang->kode_inventaris); ?>">
                                          </div>
                                </div> 
                                <div class="form-row">
                                          <div class="form-group col-md-4">
                                            <label for="jumlah">Jumlah</label>
                                            <input name="jumlah" type="int" class="form-control" id="jumlah" value="<?php echo e($barang->jumlah); ?>">
                                          </div>
                                </div> 
                                <div class="form-row">
                                          <div class="form-group col-md-4">
                                            <label for="tanggal_register">Tanggal Register</label>
                                            <input name="tanggal_register" type="date" class="form-control" id="tanggal_register" value="<?php echo e($barang->tanggal_register); ?>">
                                          </div>
                                </div> 
                                
                </div>
            <p></p>
              <button class="btn btn-warning">Update Data</button>
                <button type="submit" class="btn btn-danger"><a href="/dashboard/jenis" style="color:red;">Cancel</a></button>
            
</body>

    <!--   Core JS Files   -->
    <script src="../../assets/js/jquery.3.2.1.min.js" type="text/javascript"></script>
	<script src="../../assets/js/bootstrap.min.js" type="text/javascript"></script>

	<!--  Charts Plugin -->
	<script src="../..assets/js/chartist.min.js"></script>

    <!--  Notifications Plugin    -->
    <script src="../../assets/js/bootstrap-notify.js"></script>

    <!-- Light Bootstrap Table Core javascript and methods for Demo purpose -->
	<script src="../../assets/js/light-bootstrap-dashboard.js?v=1.4.0"></script>

	<!-- Light Bootstrap Table DEMO methods, don't include it in your project! -->
	<script src="../../assets/js/demo.js"></script>

	<script type="text/javascript">
    	$(document).ready(function(){

        	demo.initChartist();

        	// $.notify({
         //    	icon: 'pe-7s-hand',
         //    	message: "Welcome to <b>Invenity App</b> - an easy vibes for borrowing the inventory."

         //    }
         ({
                type: 'info',
                timer: 4000
            });

    	});
	</script>

</html>
<?php /**PATH C:\Users\LEN-RPL01 student\Documents\DesyfaAina\be_pwpb_xiirpl_1920_12_desyfa\resources\views/Administrator/EditDataInventaris.blade.php ENDPATH**/ ?>