<?php

namespace App;

use Check;
use Illuminate\Database\Eloquent\Model;

class Pegawai extends Model
{
    protected $table = "pegawai";
    protected $fillable = ['id','nama','alamat','gambar','nip','updated_at','created_at','user_id'];
    protected $primaryKey = 'id';
    // protected $keyType = "string";

    public function FunctionName(Type $var = null)
    {
        $this->bootIfNotBooted();
        $this->fill($attributes);
        $this->setConnection(Check::connection());   
    }
    public function getAvatar()
    {
        if(!$this->gambar){
            return asset('images/default.png');
        }
        return asset('images/',$this->gambar);
    }
    public function peminjaman()
    {
        return $this->belongsTo('App\Peminjaman');
    }
    public function joinpinjam()
    {
        return $this->belongsTo('App\joinpinjam');
    }
}
