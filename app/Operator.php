<?php

namespace App;

use Check;
use Illuminate\Database\Eloquent\Model;

class Operator extends Model
{
    protected $table = "petugas";
    protected $fillable = ['id','nama','gambar','user_id','updated_at','created_at'];
    protected $primaryKey = 'id';
    protected $keyType = "string";

    public function FunctionName(Type $var = null)
    {
        $this->bootIfNotBooted();
        $this->fill($attributes);
        $this->setConnection(Check::connection());   
    }
    public function getAvatar()
    {
        if(!$this->gambar){
            return asset('images/default.png');
        }
        return asset('images/',$this->gambar);
    }
    public function barang()
    {
        return $this->belongsTo('App\Barang');
    }
}
