<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class joinpinjam extends Model
{
    protected $table = "peminjamanview";
    protected $fillable = ['id_peminjaman','id_inventaris','jumlah','id_pinjam','tanggal_pinjam','tanggal_kembali','status_peminjaman',
    'id_pegawai','id_detail_pinjam','updated_at','created_at'];
    protected $primaryKey = 'id_peminjaman'; 
    protected $keyType = 'string';
    public function FunctionName(Type $var = null)
    {
        $this->bootIfNotBooted();
        $this->fill($attributes);
        $this->setConnection(Check::connection());   
    }
    public function detail()
    {
        return $this->hasMany('App\DetailPinjam');
    }
    public function barang()
    {
        return $this->hasMany('App\Barang');
    }
    public function pegawai()
    {
        return $this->hasMany('App\Pegawai');
    }
}
