<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DetailPinjam extends Model
{
    protected $table = "detail";
    protected $fillable = ['id','nama_pegawai','id_inventaris','jumlah','updated_at','created_at'];
    protected $primaryKey = 'id';
    // protected $keyType = 'string';
    public function FunctionName(Type $var = null)
    {
        $this->bootIfNotBooted();
        $this->fill($attributes);
        $this->setConnection(Check::connection());   
    }
    public function peminjaman()
    {
        return $this->belongsTo('App\Peminjaman');
    }
    public function joinpinjam()
    {
        return $this->belongsTo('App\joinpinjam');
    }
}
