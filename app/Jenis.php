<?php

namespace App;

use Check;
use Illuminate\Database\Eloquent\Model;

class Jenis extends Model
{
    protected $table = "jenis";
    protected $fillable = ['id','nama_jenis','kode_jenis','keterangan','updated_at','created_at'];
    protected $primaryKey = 'id';
    // protected $keyType = 'string';
    public function FunctionName(Type $var = null)
    {
        $this->bootIfNotBooted();
        $this->fill($attributes);
        $this->setConnection(Check::connection());   
    }
    public function barang()
    {
        return $this->belongsTo('App\Barang');
    }
}
