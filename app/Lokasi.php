<?php

namespace App;

use Check;
use Illuminate\Database\Eloquent\Model;

class Lokasi extends Model
{
    protected $table = "ruang";
    protected $fillable = ['id','nama_ruang','kode_ruang','updated_at','created_at'];
    protected $primaryKey = 'id';
    // protected $keyType = 'string';
    public function FunctionName(Type $var = null)
    {
        $this->bootIfNotBooted();
        $this->fill($attributes);
        $this->setConnection(Check::connection());   
    }
    public function barang()
    {
        return $this->belongsTo('App\Barang');
    }
}
