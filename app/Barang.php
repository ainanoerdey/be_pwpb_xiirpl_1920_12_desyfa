<?php

namespace App;

use Check;
use Illuminate\Database\Eloquent\Model;

class Barang extends Model
{
    protected $table = "inventaris";
    protected $fillable = ['id','id_jenis','id_ruang','id_petugas','nama','gambar','kondisi','keterangan','jumlah','tanggal_register','kode_inventaris','updated_at','created_at'];
    protected $primaryKey = 'id';
    // protected $keyType = 'integer';
    public function FunctionName(Type $var = null)
    {
        $this->bootIfNotBooted();
        $this->fill($attributes);
        $this->setConnection(Check::connection());   
    }
    public function ruang()
    {
        return $this->hasMany('App\Lokasi');
    }
    public function jenis()
    {
        return $this->hasMany('App\Jenis');
    }
    public function petugas()
    {
        return $this->hasMany('App\Operator');
    }
    public function pengembalian()
    {
        return $this->belongsTo('App\Pengembalian');
    }
    public function peminjaman()
    {
        return $this->belongsTo('App\joinpinjam');
    }
}
