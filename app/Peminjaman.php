<?php

namespace App;

use Check;
use Illuminate\Database\Eloquent\Model;

class Peminjaman extends Model
{
    protected $table = "peminjaman";
    protected $fillable = ['id','jumlah','id_inventaris','tanggal_pinjam','tanggal_kembali','status_peminjaman','id_pegawai','id_detail_pinjam','updated_at','created_at'];
    protected $primaryKey = 'id';

    public function FunctionName(Type $var = null)
    {
        $this->bootIfNotBooted();
        $this->fill($attributes);
        $this->setConnection(Check::connection());   
    }    
    public function detail_pinjam()
    {
        return $this->hasMany('App\DetailPinjam');
    }
    public function pegawai()
    {
        return $this->hasMany('App\Pegawai');
    }
    public function pengembalian()
    {
        return $this->belongsTo('App\Pengembalian');
    }
    public function status()
    {
        return $this->hasMany('App\Status');
    }
}
