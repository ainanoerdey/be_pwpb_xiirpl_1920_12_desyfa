<?php

namespace App\Providers;
use App\Pegawai;
use Illuminate\Support\ServiceProvider;


class PegawaiClassname extends ServiceProvider
{
    public function boot()
    {
        view()->composer('*',function($view){
            $view->with('datapegawai', Pegawai::all());
        });
    }
}
