<?php

namespace App\Providers;
use App\Operator;
use Illuminate\Support\ServiceProvider;


class PetugasClassname extends ServiceProvider
{
    public function boot()
    {
        view()->composer('*',function($view){
            $view->with('dataop', Operator::all());
        });
    }
}
