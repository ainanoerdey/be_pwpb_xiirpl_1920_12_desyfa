<?php

namespace App\Providers;
use App\Barang;
use Illuminate\Support\ServiceProvider;


class BarangClassname extends ServiceProvider
{
    public function boot()
    {
        view()->composer('*',function($view){
            $view->with('databarang', Barang::all());
        });
    }
}
