<?php

namespace App\Providers;
use App\Jenis;
use Illuminate\Support\ServiceProvider;


class JenisClassname extends ServiceProvider
{
    public function boot()
    {
        view()->composer('*',function($view){
            $view->with('datajenis', Jenis::all());
        });
    }
}
