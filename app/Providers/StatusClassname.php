<?php

namespace App\Providers;
use App\Status;
use Illuminate\Support\ServiceProvider;


class StatusClassname extends ServiceProvider
{
    public function boot()
    {
        view()->composer('*',function($view){
            $view->with('status', Status::all());
        });
    }
}
