<?php

namespace App\Providers;
use App\Pengembalian;
use Illuminate\Support\ServiceProvider;


class PengembalianClassname extends ServiceProvider
{
    public function boot()
    {
        view()->composer('*',function($view){
            $view->with('datakembali', Pengembalian::all());
        });
    }
}
