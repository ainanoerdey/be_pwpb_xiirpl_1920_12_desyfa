<?php

namespace App\Providers;
use App\Peminjaman;
use Illuminate\Support\ServiceProvider;


class PeminjamanClassname extends ServiceProvider
{
    public function boot()
    {
        view()->composer('*',function($view){
            $view->with('datapinjam', Peminjaman::all());
        });
    }
}
