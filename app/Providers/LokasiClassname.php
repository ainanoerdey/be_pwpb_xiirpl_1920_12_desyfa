<?php

namespace App\Providers;
use App\Lokasi;
use Illuminate\Support\ServiceProvider;


class LokasiClassname extends ServiceProvider
{
    public function boot()
    {
        view()->composer('*',function($view){
            $view->with('datalokasi', Lokasi::all());
        });
    }
}
