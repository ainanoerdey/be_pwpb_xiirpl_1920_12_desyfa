<?php

namespace App\Providers;
use App\DetailPinjam;
use Illuminate\Support\ServiceProvider;


class DetailClassname extends ServiceProvider
{
    public function boot()
    {
        view()->composer('*',function($view){
            $view->with('detail', DetailPinjam::all());
        });
    }
}
