<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

// panggil model pegawai
use App\Pegawai;
use PDF;
use DataTables;


class PegawaiController extends Controller
{
    public function json(){
        return Datatables::of(Pegawai::all())->make(true);
    }
  
    public function index()
    {
    	// mengambil data pegawai
    	$pegawai = Pegawai::all();

    	// mengirim data pegawai ke view pegawai
    	return view('Administrator/pegawai', ['pegawai' => $pegawai]);
    }
    public function create(Request $request)
    {
       
        $user = new \App\User;
        $user->role = "Pegawai";
        $user->nama = $request->nama;
        $user->password = bcrypt('rahasia');
        $user->remember_token = str_random(60);
        $user->save();

        $request->request->add(['user_id' => $user->id ]);
        $pegawai = \App\Pegawai::create($request->all());
        if($request->hasFile('gambar')){
            $request->file('gambar')->move('images/',$request->file('gambar')->getClientOriginalName());
            $pegawai->gambar = $request->file('gambar')->getClientOriginalName();
            $pegawai->save();
        }
        return redirect('dashboard/user/pegawai')->with('sukses','Data berhasil ditambahkan'); 
    }
    public function insert(Request $request)
    {
        return view('Administrator/TambahPegawai'); 
    }
    
    public function delete($id)
	{
    	$pegawai = Pegawai::find($id);
    	$pegawai->delete();
    	return redirect()->back();
    }
    public function show(Request $request,$id)
    {
        $pegawai = Pegawai::find($id);
        return view('Administrator/DetailPegawai', ['pegawai' => $pegawai]);
    }
    public function cetak_pdf()
    {
    	$pegawai = Pegawai::all();
 
    	$pdf = PDF::loadview('Administrator/pegawaipdf',['pegawai'=>$pegawai]);
    	return $pdf->download('laporan-pegawai-pdf');
    }
    public function edit($id)
    {
        $pegawai = \App\Pegawai::find($id);
        return view('Administrator/EditPegawai', compact(['pegawai']));
    }
    public function update(Request $request, $id)
    {
        //dd($request->all());
        $pegawai = \App\Pegawai::find($id);
        $pegawai->update($request->all());
        if($request->hasFile('gambar')){
            $request->file('gambar')->move('images/',$request->file('gambar')->getClientOriginalName());
            $pegawai->gambar = $request->file('gambar')->getClientOriginalName();
            $pegawai->save();
        }
        return redirect('dashboard/user/pegawai')->with('sukses','Data berhasil di update'); 
    }
    
}