<?php

namespace App\Http\Controllers;

use View;
use Illuminate\Http\Request;
use App\Barang;
use App\Operator;
use App\Pegawai;

class DashboardController extends Controller
{
    public function admin()
    {
    return view('/Administrator/Dashboard');
    }
    public function op()
    {
    return view('/Operator/Dashboard');
    }
    public function pegawai()
    {
            $barang = Barang::count();
            $pegawai = Pegawai::count();
            $petugas = Operator::count();
            return View::make('Pegawai.Dashboard')
            ->with(compact('barang'))
            ->with(compact('pegawai'))
            ->with(compact('petugas'));
    }
    public function index()
    {
        $barang = Barang::count();
        $pegawai = Pegawai::count();
        $petugas = Operator::count();
        return View::make('Dashboard')
        ->with(compact('barang'))
        ->with(compact('pegawai'))
        ->with(compact('petugas'));
    }
    public function user()
    {
    	return view('/Administrator/TableOperator');
    }
    public function inventory()
    {
    	return view('/Administrator/TableInventaris');
    }
    public function loan()
    {
    	return view('/Administrator/LoanData');
    }
 	  public function return()
    {
    	return view('/Administrator/ReturnData');
    }
    public function account()
    {
    	return view('/Administrator/Setting');
    }
    public function lokasi()
    {
    	return view('/Administrator/TableLokasi');
    }
    public function jenis()
    {
    	return view('/Administrator/TableJenis');
    }
    public function setting()
    {
    	return view('/Administrator/Setting');
    }
}
