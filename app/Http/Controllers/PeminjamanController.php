<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Peminjaman;
use App\Barang;
use App\Pegawai;
use App\Operator;
use App\DetailPinjam;
use PDF;

class PeminjamanController extends Controller
{
    public function json(){
        return Datatables::of(Peminjaman::all())->make(true);
    }
  
    public function index()
    {
    	// mengambil data pegawai
    	$peminjaman = Peminjaman::all();

    	// mengirim data pegawai ke view pegawai
    	return view('Administrator/LoanData', ['peminjaman' => $peminjaman]);
    }
    
    public function cetak_pdf()
    {
    	$peminjaman = Peminjaman::all();
 
    	$pdf = PDF::loadview('Administrator/pinjampdf',['peminjaman'=>$peminjaman]);
    	return $pdf->download('laporan-peminjaman-pdf');
    }
    public function create(Request $request)
    {
        \App\Peminjaman::create($request->all());
        return redirect('/dashboard/loan')->with('sukses','Data berhasil ditambahkan'); 
    }
    public function insert()
    {
        $detail = DetailPinjam::all();
        $barang = Barang::all();
        $pegawai = Pegawai::all();
        $petugas = Operator::all(); 
        $peminjaman = Peminjaman::all();
        return view('Peminjaman.tambah', compact('barang','pegawai','petugas','peminjaman'));
    }
    public function edit($id)
    {
        $peminjaman = \App\Peminjaman::find($id);
        return view('Administrator/editloan')->with('peminjaman', $peminjaman);
    }
    public function update(Request $request, $id)
    {
        $peminjaman = \App\Peminjaman::find($id);
        $peminjaman->update($request->all());
        return redirect('dashboard/loan')->with('sukses','Data berhasil ditambahkan'); 
        
    }
    public function show($id)
    {
        $peminjaman = Peminjaman::where('id_pinjam',$id)->first();
        return view('Administrator.DetailPeminjaman', ['peminjaman' => $peminjaman]);
        // $produk = produk::where('id',$id)->first();
    }
}
