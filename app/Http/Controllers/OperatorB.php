<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Barang;

class OperatorB extends Controller
{
    public function setting()
    {
    	return view('/Operator/Setting');
    }
    public function show(Request $request,$id)
    {
        $barang = Barang::find($id);
        return view('Operator/DetailBarang', ['barang' => $barang]);
    }
    public function index()
    {
    	// mengambil data jenis
    	$barang = Barang::all();

    	// mengirim data jenis ke view jenis
    	return view('Operator/TableInventaris', ['barang' => $barang]);
    }
}
