<?php

namespace App\Http\Controllers;
use App\Lokasi;
use Illuminate\Http\Request;
use PDF;

class LokasiController extends Controller
{
    public function index()
    {
    	// mengambil data pegawai
    	$lokasi = Lokasi::all();

    	// mengirim data pegawai ke view pegawai
    	return view('Administrator/TableLokasi', ['lokasi' => $lokasi]);
    }
    public function create(Request $request)
    {
        \App\Lokasi::create($request->all());
        return redirect('dashboard/lokasi')->with('sukses','Data berhasil ditambahkan'); 
    }
    public function insert(Request $request)
    {
        return view('Administrator/TambahDataLokasi'); 
    }
    public function edit($id)
    {
        $lokasi = \App\Lokasi::find($id);
        return view('Administrator/EditDatalokasi')->with('lokasi', $lokasi);
    }
    public function update(Request $request, $id)
    {
        $lokasi = \App\Lokasi::find($id);
        $lokasi->update($request->all());
        return redirect('dashboard/lokasi')->with('sukses','Data berhasil ditambahkan'); 
        
    }
    public function show(Request $request,$id)
    {
        $lokasi = Lokasi::find($id);
        return view('Administrator/DetailLokasi', ['lokasi' => $lokasi]);
    }
    public function delete($id)
	{
    	$lokasi = Lokasi::find($id);
    	$lokasi->delete();
    	return redirect()->back();
    }
    public function cetak_pdf()
    {
    	$lokasi = Lokasi::all();
 
    	$pdf = PDF::loadview('Administrator/ruangpdf',['lokasi'=>$lokasi]);
    	return $pdf->download('laporan-ruang-pdf');
    }

}
