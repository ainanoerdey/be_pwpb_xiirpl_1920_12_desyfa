<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Peminjaman;
use PDF;

class Pinjam_Op extends Controller
{
    public function json(){
        return Datatables::of(Peminjaman::all())->make(true);
    }
  
    public function index()
    {
    	// mengambil data pegawai
    	$peminjaman = Peminjaman::all();

    	// mengirim data pegawai ke view pegawai
    	return view('Operator/LoanData', ['peminjaman' => $peminjaman]);
    }
    
    public function cetak_pdf()
    {
    	$peminjaman = Peminjaman::all();
 
    	$pdf = PDF::loadview('Operator/pinjampdf',['peminjaman'=>$peminjaman]);
    	return $pdf->download('laporan-peminjaman-pdf');
    }
    public function show($id)
    {
        $peminjaman = Peminjaman::where('id',$id)->first();
        return view('Operator.DetailPeminjaman', ['peminjaman' => $peminjaman]);
        // $produk = produk::where('id',$id)->first();
    }
}
