<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Operator;

class OpController extends Controller
{
    public function index()
    {
    	// mengambil data pegawai
    	$petugas = Operator::all();

    	// mengirim data pegawai ke view pegawai
    	return view('Administrator/TablePetugas', ['petugas' => $petugas]);
    }
    public function create(Request $request)
    {
        $user = new \App\User;
        $user->role = "Operator";
        $user->nama = $request->nama;
        $user->password = bcrypt('rahasia');
        $user->remember_token = str_random(60);
        $user->save();

        $request->request->add(['user_id' => $user->id ]);
        $petugas = \App\Operator::create($request->all());
        if($request->hasFile('gambar')){
            $request->file('gambar')->move('images/',$request->file('gambar')->getClientOriginalName());
            $petugas->gambar = $request->file('gambar')->getClientOriginalName();
            $petugas->save();
        }
        return redirect('dashboard/user')->with('sukses','Data berhasil ditambahkan'); 
    }
    public function insert(Request $request)
    {
        return view('Administrator/TambahOperator'); 
    }
    public function show(Request $request,$id)
    {
        $petugas = Operator::find($id);
        return view('Administrator/DetailOperator', ['petugas' => $petugas]);
    }
    
    public function delete($id)
	{
    	$petugas = Operator::find($id);
    	$petugas->delete();
    	return redirect()->back();
    }
    public function edit($id)
    {
        $petugas = \App\Operator::find($id);
        return view('Administrator/EditOp', compact(['petugas']));
    }
    public function update(Request $request, $id)
    {
        //dd($request->all());
        $petugas = \App\Operator::find($id);
        $petugas->update($request->all());
        if($request->hasFile('gambar')){
            $request->file('gambar')->move('images/',$request->file('gambar')->getClientOriginalName());
            $petugas->gambar = $request->file('gambar')->getClientOriginalName();
            $petugas->save();
        }
        return redirect('dashboard/user')->with('sukses','Data berhasil di update'); 
    }
}
