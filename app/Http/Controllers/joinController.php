<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\joinpinjam;

class joinController extends Controller
{
    public function json(){
        return Datatables::of(joinpinjam::all())->make(true);
    }
  
    public function index()
    {
    	// mengambil data pegawai
    	$pinjam = joinpinjam::all();

    	// mengirim data pegawai ke view pegawai
    	return view('Administrator/LoanData', ['pinjam' => $pinjam]);
    }
    
    public function cetak_pdf()
    {
    	$pinjam = joinpinjam::all();
 
    	$pdf = PDF::loadview('Administrator/pinjampdf',['pinjam'=>$pinjam]);
    	return $pdf->download('laporan-peminjaman-pdf');
    }
    public function create(Request $request)
    {
        \App\joinpinjam::create($request->all());
        return redirect('/dashboard/loan')->with('sukses','Data berhasil ditambahkan'); 
    }
    public function insert()
    {
        $detail = DetailPinjam::all();
        $barang = Barang::all();
        $pegawai = Pegawai::all();
        $petugas = Operator::all(); 
        return view('Peminjaman.tambah', compact('barang','pegawai','petugas'));
    }
    public function show($id)
    {
        $pinjam = joinpinjam::where('id_pinjam',$id)->first();
        return view('Administrator.DetailPeminjaman', ['peminjaman' => $peminjaman]);
        // $produk = produk::where('id',$id)->first();
    }
}
