<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pengembalian;

class PengembalianController extends Controller
{
    public function json(){
        return Datatables::of(Pengembalian::all())->make(true);
    }
  
    public function index()
    {
    	// mengambil data pegawai
    	$pengembalian = Pengembalian::all();

    	// mengirim data pegawai ke view pegawai
    	return view('Administrator/ReturnData', ['pengembalian' => $pengembalian]);
    }
    
    public function cetak_pdf()
    {
    	$pengembalian = Pengembalian::all();
 
    	$pdf = PDF::loadview('Administrator/kembalipdf',['pengembalian'=>$pengembalian]);
    	return $pdf->download('laporan-pengembalian-pdf');
    }
    public function create(Request $request)
    {
        \App\Pengembalian::create($request->all());
        return redirect('/dashboard/return')->with('sukses','Data berhasil ditambahkan'); 
    }
    public function insert()
    {
        $detail = DetailPinjam::all();
        $barang = Barang::all();
        $pegawai = Pegawai::all();
        $petugas = Operator::all(); 
        $pengembalian = Pengembalian::all();
        return view('Peminjaman.tambah', compact('barang','pegawai','petugas','pengembalian'));
    }
    public function show($id)
    {
        $pengembalian = Pengembalian::where('id',$id)->first();
        return view('Operator.DetailPengembalian', ['pengembalian' => $pengembalian]);
        // $produk = produk::where('id',$id)->first();
    }
}
