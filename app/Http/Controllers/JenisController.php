<?php

namespace App\Http\Controllers;

use App\Jenis;
use PDF;
use Illuminate\Http\Request;

class JenisController extends Controller
{
    public function index()
    {
    	// mengambil data jenis
    	$jenis = Jenis::all();

    	// mengirim data jenis ke view jenis
    	return view('Administrator/TableJenis', ['jenis' => $jenis]);
    }
    public function create(Request $request)
    {
        \App\Jenis::create($request->all());
        return redirect('dashboard/jenis')->with('sukses','Data berhasil ditambahkan'); 
    }
    public function insert(Request $request)
    {
        return view('Administrator/TambahDataJenis'); 
    }
    public function edit($id)
    {
        $jenis = \App\Jenis::find($id);
        return view('Administrator/EditDataJenis', compact(['jenis']));
    }
    public function update(Request $request, $id)
    {
        $jenis = \App\Jenis::find($id);
        $jenis->update($request->all());
        return redirect('dashboard/jenis')->with('sukses','Data berhasil ditambahkan'); 
    }
    public function delete($id)
	{   
    	$jenis = Jenis::find($id);
    	$jenis->delete();
    	return redirect()->back();
    }
    public function show(Request $request,$id)
    {
        $jenis = Jenis::find($id);
        return view('Administrator/DetailJenis', ['jenis' => $jenis]);
    }
    public function cetak_pdf()
    {   
    	$jenis = Jenis::all();
 
    	$pdf = PDF::loadview('Administrator/jenispdf',['jenis'=>$jenis]);
    	return $pdf->download('laporan-jenis-pdf');
    }
    

}
