<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Barang;
use App\Lokasi;
use App\Jenis;
use App\Pegawai;
use App\DetailPinjam;

class PegawaiB extends Controller
{
    public function index()
    {
    	// mengambil data jenis
        $barang = Barang::all();
        $ruang = Lokasi::all();
        $jenis = Jenis::all();

    	// mengirim data jenis ke view jenis
    	return view('Pegawai/TableInventaris', ['barang' => $barang]);
    }
    public function show(Request $request,$id)
    {
        $barang = Barang::find($id);
        $ruang = Lokasi::all();
        $jenis = Jenis::all();
        
        return view('/Pegawai/DetailBarang', ['barang' => $barang]);
    }
    public function setting()
    {
    	return view('/Pegawai/Setting');
    }
    public function create(Request $request)
    {
        \App\Peminjaman::create($request->all());
        return redirect('/inventory')->with('sukses','Data berhasil ditambahkan'); 
    }
    public function insert()
    {
        $detail = DetailPinjam::all();
        $barang = Barang::all();
        $pegawai = Pegawai::all();

        return view('Peminjaman.tambah', compact('barang','pegawai','petugas'));
    }
}
