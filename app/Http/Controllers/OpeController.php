<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Peminjaman;
use PDF;


class OpeController extends Controller
{
    public function loan()
    {
    	return view('/Operator/LoanData');
    }
 	  public function return()
    {
    	return view('/Operator/ReturnData');
    }
    public function json(){
        return Datatables::of(Peminjaman::all())->make(true);
    }
  
    public function index()
    {
    	// mengambil data pegawai
    	$peminjaman = Peminjaman::all();

    	// mengirim data pegawai ke view pegawai
    	return view('Operator/LoanData', ['peminjaman' => $peminjaman]);
    }
    public function edit($id)
    {
        $peminjaman = \App\Peminjaman::find($id);
        return view('Operator/editloan')->with('peminjaman', $peminjaman);
    }
    public function update(Request $request, $id)
    {
        $peminjaman = \App\Peminjaman::find($id);
        $peminjaman->update($request->all());
        return redirect('dashboard/loanop')->with('sukses','Data berhasil ditambahkan'); 
        
    }
    
    public function cetak_pdf()
    {
    	$peminjaman = Peminjaman::all();
 
    	$pdf = PDF::loadview('Operator/pinjampdf',['peminjaman'=>$peminjaman]);
    	return $pdf->download('laporan-peminjaman-pdf');
    }

}
