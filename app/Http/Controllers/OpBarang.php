<?php

namespace App\Http\Controllers;

use App\Barang;
use PDF;
use Illuminate\Http\Request;

class OpBarang extends Controller
{
    public function index()
    {
    	// mengambil data jenis
    	$barang = Barang::all();

    	// mengirim data jenis ke view jenis
    	return view('Operator/TableInventaris', ['barang' => $barang]);
    }

    public function show(Request $request,$id)
    {
        $barang = Barang::find($id);
        return view('Operator/DetailBarang', ['barang' => $barang]);
    }

}
