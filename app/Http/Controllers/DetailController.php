<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Peminjaman;
use App\Barang;
use App\Pegawai;
use App\Operator;
use App\DetailPinjam;
use PDF;

class DetailController extends Controller
{
    public function json(){
        return Datatables::of(Peminjaman::all())->make(true);
    }
  
    public function index()
    {
    	// mengambil data pegawai
    	$detail = DetailPinjam::all();

    	// mengirim data pegawai ke view pegawai
    	return view('Administrator/Detail', ['detail' => $detail]);
    }
    
    public function cetak_pdf()
    {
    	$detail = Detail::all();
 
    	$pdf = PDF::loadview('Administrator/detailpdf',['detail'=>$detail]);
    	return $pdf->download('laporan-detailpinjam-pdf');
    }
    public function create(Request $request)
    {
        \App\DetailPinjam::create($request->all());
        return redirect('/dashboard/detail')->with('sukses','Data berhasil ditambahkan'); 
    }
    public function insert()
    {
        $detail = DetailPinjam::all();
        $barang = Barang::all();
        $pegawai = Pegawai::all();
        return view('Peminjaman.tambahdetail', compact('barang','pegawai','detail'));
    }
}
