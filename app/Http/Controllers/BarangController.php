<?php

namespace App\Http\Controllers;

use App\Barang;
use App\Lokasi;
use Alert;
use PDF;
use Illuminate\Http\Request;

class BarangController extends Controller
{
    public function index()
    {
    	// mengambil data jenis
    	$barang = Barang::all();

    	// mengirim data jenis ke view jenis
    	return view('Administrator/TableInventaris', ['barang' => $barang]);
    }
    public function create(Request $request)
    {
        \App\Barang::create($request->all());
        if($request->hasFile('gambar')){
            $request->file('gambar')->move('images/',$request->file('gambar')->getClientOriginalName());
            $barang->gambar = $request->file('gambar')->getClientOriginalName();
            $barang->save();
        }
        return redirect('dashboard/inventory')->with('sukses','Data berhasil ditambahkan'); 
    }
    public function insert(Request $request)
    {
        return view('Administrator/TambahDataInventaris'); 
    }
    public function edit($id)
    {
        $barang = \App\Barang::find($id);
        $lokasi = \App\Lokasi::find($id);
        return view('Administrator/EditDataInventaris', compact(['barang']));
    }
    public function update(Request $request, $id)
    {
        $barang = \App\Barang::find($id);
        $barang->update($request->all());
        if($request->hasFile('gambar')){
            $request->file('gambar')->move('images/',$request->file('gambar')->getClientOriginalName());
            $barang->gambar = $request->file('gambar')->getClientOriginalName();
            $barang->save();
        }
        return redirect('dashboard/inventory')->with('sukses','Data berhasil diupdate'); 
    }
    public function delete($id)
	{
    	$barang = Barang::find($id);
    	$barang->delete();
    	return redirect('dashboard/inventory')->with('sukses','Data berhasil dihapus'); ;
    }
    public function show(Request $request,$id)
    {
        $barang = Barang::find($id);
        return view('Administrator/DetailBarang', ['barang' => $barang]);
    }
    public function cetak_pdf()
    {
    	$barang = Barang::all();
 
    	$pdf = PDF::loadview('Administrator/barangpdf',['barang'=>$barang]);
    	return $pdf->download('laporan-barang-pdf');
    }
    

}
