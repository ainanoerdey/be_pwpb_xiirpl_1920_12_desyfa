<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\User;

class AuthController extends Controller
{
    public function login()
    {   
        return view('index');
    }
    public function postlogin(Request $request)
    {
        // dd($request->all());
        if (Auth::attempt($request->only('nama','password'))) {
            // return redirect('/dashboarda');
            $user = User::find(Auth::id());
            if($user->role == 'admin'){
                return redirect('/dashboarda');
            }
            elseif($user->role == 'op'){
                return redirect('/dashboard/op') ;
            }
            elseif($user->role == 'pegawai'){
                return redirect('/dashboardb');
            }
        }
        return redirect('/login');
    }
    public function logout()
    {
        Auth::logout();
        return redirect('/login');
    }
}