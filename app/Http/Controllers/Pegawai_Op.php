<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use PDF;
use App\Pegawai;

class Pegawai_Op extends Controller
{
    public function json(){
        return Datatables::of(Pegawai::all())->make(true);
    }
  
    public function index()
    {
    	// mengambil data pegawai
    	$pegawai = Pegawai::all();

    	// mengirim data pegawai ke view pegawai
    	return view('Operator/pegawai', ['pegawai' => $pegawai]);
    }
    public function create(Request $request)
    {
       
        $user = new \App\User;
        $user->role = "Pegawai";
        $user->nama = $request->nama;
        $user->password = bcrypt('rahasia');
        $user->remember_token = str_random(60);
        $user->save();

        $request->request->add(['user_id' => $user->id ]);
        $pegawai = \App\Pegawai::create($request->all());
        return redirect('op/user/pegawai')->with('sukses','Data berhasil ditambahkan'); 
    }
    public function insert(Request $request)
    {
        return view('Operator/TambahPegawai'); 
    }
    
    public function delete($id)
	{
    	$pegawai = Pegawai::find($id);
    	$pegawai->delete();
    	return redirect()->back();
    }
    public function show(Request $request,$id)
    {
        $pegawai = Pegawai::find($id);
        return view('Operator/DetailPegawai', ['pegawai' => $pegawai]);
    }
    public function cetak_pdf()
    {
    	$pegawai = Pegawai::all();
 
    	$pdf = PDF::loadview('Administrator/pegawaipdf',['pegawai'=>$pegawai]);
    	return $pdf->download('laporan-pegawai-pdf');
    }
    
}