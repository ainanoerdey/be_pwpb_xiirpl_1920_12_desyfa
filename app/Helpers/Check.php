<?php

namespace App\Helpers;

use App\User;
use Illuminate\Support\Facades\Auth;

class Check{

    public static function connection()
    {
        if(Auth::id()) {
            $user = User::find(Auth::id());
            if($user->role == 'admin'){
                return 'mysql';
            }
            elseif($user->role == 'operator'){
                return 'mysql_operator';
            }
            elseif($user->role == 'pegawai'){
                return 'mysql_pegawai';
            }
        } else {
            return mysql;
        }
    }
}