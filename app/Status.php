<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Status extends Model
{
    protected $table = "status";
    protected $fillable = ['id','status_peminjaman'];
    protected $primaryKey = 'id';
    // protected $keyType = "string";

    public function FunctionName(Type $var = null)
    {
        $this->bootIfNotBooted();
        $this->fill($attributes);
        $this->setConnection(Check::connection());   
    }
    public function status()
    {
        return $this->belongsTo('App\Peminjaman');
    }
}
