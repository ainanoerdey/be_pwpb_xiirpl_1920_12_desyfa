<?php

namespace App;

use Check;
use Illuminate\Database\Eloquent\Model;

class Pengembalian extends Model
{
    protected $table = "pengembalian";
    protected $fillable = ['id','id_inventaris','jumlah','tanggal_kembali','id_peminjaman','nama_pegawai','updated_at','created_at'];
    protected $primaryKey = 'id';
    // protected $keyType = "string";

    public function FunctionName(Type $var = null)
    {
        $this->bootIfNotBooted();
        $this->fill($attributes);
        $this->setConnection(Check::connection());   
    }
    public function barang()
    {
        return $this->hasMany('App\Barang');
    }
    public function peminjaman()
    {
        return $this->hasMany('App\Peminjaman');
    }    
}
