<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToInventarisTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('inventaris', function(Blueprint $table)
		{
			$table->foreign('id_jenis', 'id_jenis')->references('id')->on('jenis')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('id_petugas', 'id_petugas')->references('id')->on('petugas')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('id_ruang', 'id_ruang')->references('id')->on('ruang')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('inventaris', function(Blueprint $table)
		{
			$table->dropForeign('id_jenis');
			$table->dropForeign('id_petugas');
			$table->dropForeign('id_ruang');
		});
	}

}
