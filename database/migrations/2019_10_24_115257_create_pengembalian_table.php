<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePengembalianTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('pengembalian', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('id_peminjaman')->index('id_peminjaman');
			$table->string('nama_pegawai', 30);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('pengembalian');
	}

}
