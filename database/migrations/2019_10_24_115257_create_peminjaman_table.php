<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePeminjamanTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('peminjaman', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->date('tanggal_pinjam');
			$table->date('tanggal_kembali');
			$table->integer('id_pegawai');
			$table->string('status', 30);
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('peminjaman');
	}

}
