<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateInventarisTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('inventaris', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('id_jenis')->index('id_jenis');
			$table->integer('id_ruang')->index('id_ruang');
			$table->integer('id_petugas')->index('id_petugas');
			$table->string('nama', 30);
			$table->string('kondisi', 30);
			$table->text('keterangan', 65535);
			$table->integer('jumlah');
			$table->date('tanggal_register');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('inventaris');
	}

}
