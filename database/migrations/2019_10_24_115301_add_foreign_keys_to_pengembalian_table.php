<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToPengembalianTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('pengembalian', function(Blueprint $table)
		{
			$table->foreign('id_peminjaman', 'id_peminjaman')->references('id')->on('peminjaman')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('pengembalian', function(Blueprint $table)
		{
			$table->dropForeign('id_peminjaman');
		});
	}

}
