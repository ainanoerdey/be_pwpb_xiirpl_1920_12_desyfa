<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDetailTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('detail', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('nama_pegawai', 30);
			$table->integer('id_inventaris')->index('id_inventaris');
			$table->integer('jumlah');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('detail');
	}

}
