<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateRuangTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('ruang', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('nama_ruang', 30);
			$table->string('kode_ruang', 30);
			$table->text('keterangan', 65535);
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('ruang');
	}

}
