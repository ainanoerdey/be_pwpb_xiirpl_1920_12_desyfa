<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToDetailTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('detail', function(Blueprint $table)
		{
			$table->foreign('id_inventaris', 'id_inventaris')->references('id')->on('inventaris')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('detail', function(Blueprint $table)
		{
			$table->dropForeign('id_inventaris');
		});
	}

}
