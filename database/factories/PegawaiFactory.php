<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(Model::class, function (Faker $faker) {
    return [
        'id_pegawai' => '',
        'nama' => $faker->name,
        'nip' => ''
    ];
});
