<!DOCTYPE html>
<html>
<head>
	<title>PDF</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
	<style type="text/css">
		table tr td,
		table tr th{
			font-size: 9pt;
		}
	</style>
	<center>
		<p>Data Pegawai</p>
	</center>
	<table class='table table-bordered'>
    <thead>
        <tr>
          <th>ID PEGAWAI</th>
          <th>NAMA </th>
          <th>NIP</th>
        </tr>
        </thead>
        <tbody>
        @foreach($pegawai as $p)
        <tr>
          <td>{{ $p->id_pegawai }}</td>
          <td>{{ $p->nama }}</td>
          <td>{{ $p->nip }}</td>
        </tr>
        @endforeach
        </tbody>
	</table>

</body>
</html>