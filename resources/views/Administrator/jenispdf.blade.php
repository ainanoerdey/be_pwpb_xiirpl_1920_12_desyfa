<!DOCTYPE html>
<html>
<head>
	<title>PDF</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
	<style type="text/css">
		table tr td,
		table tr th{
			font-size: 9pt;
		}
	</style>
	<center>
		<p>Data Jenis Inventaris</p>
	</center>
	<table class='table table-bordered'>
    <thead>
        <tr>
            <th>ID JENIS</th>
            <th>NAMA</th>
        </tr>
        </thead>
        <tbody>
        @foreach($jenis as $j)
        <tr>
            <td>{{ $j->id_jenis }}</td>
            <td>{{ $j->nama_jenis }}</td>
        </tr>
        @endforeach
        </tbody>
	</table>

</body>
</html>