<!DOCTYPE html>
<html>
<head>
	<title>PDF</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
	<style type="text/css">
		table tr td,
		table tr th{
			font-size: 9pt;
		}
	</style>
	<center>
		<p>Data Inventaris</p>
	</center>
	<table class='table table-bordered'>
    <thead>
        <tr>
            <th>ID INVENTARIS</th>
            <th>NAMA</th>
            <th>JUMLAH</th>
            <th>KONDISI</th>
            <th>KODE</th>
         </tr>
    </thead>
        <tbody>
        @foreach($barang as $b)
        <tr>
            <td>{{ $b->id_inventaris }}</td>
            <td>{{ $b->nama }}</td>
            <td>{{ $b->jumlah }}</td>
            <td>{{ $b->kondisi }}</td>
            <td>{{ $b->kode_inventaris}}</td>
        </tr>
        @endforeach
        </tbody>
	</table>

</body>
</html>