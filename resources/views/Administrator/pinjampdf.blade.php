<!DOCTYPE html>
<html>
<head>
	<title>PDF</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
	<style type="text/css">
		table tr td,
		table tr th{
			font-size: 9pt;
		}
	</style>
	<center>
		<p>Data Peminjaman</p>
	</center>
	<table class='table table-bordered'>
  <thead>
  <tr>
    <th>ID PEMINJAMAN</th>
    <th>ID INVENTARIS</th>
    <th>TANGGAL PINJAM</th>
    <th>TANGGAL KEMBALI</th>
  </tr>
  </thead>
  <tbody>
  <tr>
  @foreach($peminjaman as $pj)
    <td>{{$pj->id_peminjaman}}</td>
    <td>{{$pj->id_inventaris}}</td>
    <td>{{$pj->tanggal_pinjam}}</td>
    <td>{{$pj->tanggal_kembali}}</td>
  </tr>
  @endforeach
  </tbody>                     
	</table>

</body>
</html>