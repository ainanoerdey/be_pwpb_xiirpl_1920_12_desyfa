<!doctype html>
<html lang="en">
<head>

	<meta charset="utf-8" />
	<link rel="icon" type="image/png" href="assets/img/favicon.ico">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

	<title>invenity.org</title>

	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />

    <link href="{{ asset('/css/bootstrap.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('/css/animate.min.css') }}" rel="stylesheet"/>
    <link href="{{ asset('/css/bootstrap.css?v=1.4.0') }}" rel="stylesheet"/>
    <link href="{{ asset('/css/demo.css') }}" rel="stylesheet" />
    <!--     Fonts and icons     -->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300' rel='stylesheet' type='text/css'>
    <link href="{{ asset('/css/pe-icon-7-stroke.css') }}" rel="stylesheet" />

</head>
<body>
<div class="wrapper" style="margin-left:40vh;">
    <div class="sidebar">

    	<div class="sidebar-wrapper">
            <div class="logo">
            <img src="{{asset('/img/profile.png') }}" style="width: 15vh; height: 15vh; margin-left: 9vh;">
                <h6 class="text-center">{{ auth()->user()->nama }}</h6>
            </div>
            <ul class="nav">
            <li>
                    <a href="{{route('dashboardpgw')}}">
                        <i class="pe-7s-home"></i>
                        <p>Home</p>
                    </a>
                </li>
                <li>
                    <a href="{{route('barangpgw')}}">
                        <i class="pe-7s-news-paper"></i>
                        <p>Inventory </p>
                    </a>
                </li>
                <li>
                    <a href="{{route('settingpgw')}}">
                        <i class="pe-7s-settings"></i>
                        <p>Account Setting</p>
                    </a>
                </li>
				<li class="active-pro">
                    <a href="{{route('logout')}}">
                        <i class="pe-7s-rocket"></i>
                        <p>Log Out</p>
                    </a>
                </li>
            </ul>
    	</div>
    </div>


        <div class="container" style="margin-top:5%;">
            <div class="row">
                 <div class="card col-sm-4" style="width: 20rem; margin-top: 2vh; margin-left:2vh;">
                         <div class="card-body">
                           <h3 class="card-title" style="text-align:center; color: darkgreen">200</h3>
                           <h6 class="card-subtitle mb-2 text-muted" style="text-align:center; margin-bottom: 2vh;">Inventory</h6>
                          
                         </div>
                 </div>
                 <div class="card col-sm-4" style="width: 20rem; margin-top: 2vh; margin-left:2vh;">
                         <div class="card-body">
                           <h3 class="card-title" style="text-align:center; color: red">2</h3>
                           <h6 class="card-subtitle mb-2 text-muted" style="text-align:center; margin-bottom: 2vh;">Operator</h6>
                          
                         </div>
                 </div>
                 <div class="card col-sm-4" style="width: 20rem; margin-top: 2vh; margin-left:2vh;">
                         <div class="card-body">
                           <h3 class="card-title" style="text-align:center; color:rgb(53, 50, 205)">50</h3>
                           <h6 class="card-subtitle mb-2 text-muted" style="text-align:center; margin-bottom: 2vh;">Borrowwer</h6>
                          
                         </div>
                 </div>
         </div>
         
     </div>
     <div class="container"> 
                <div class="row">
                <form action="{{ route('pengembalian') }}" method="post">
                                @csrf
                                
                                <div class="form-row">
                                          <div class="form-group col-md-4">
                                            <label for="id_peminjaman">Id Peminjaman</label>
                                            <select name="id_peminjaman" id="id_peminjaman" class="form-control mt-1">
                                              @foreach($datapinjam as $p)
                                                <option>{{ $p->id }}</option>
                                              @endforeach
                                            </select>
                                            </div>
                                </div>  
                                <div class="form-row">
                                          <div class="form-group col-md-4">
                                            <label for="nama_pegawai">Nama Pegawai</label>
                                            <input name="nama_pegawai" type="text" class="form-control" id="nama_pegawai">
                                          </div>
                                </div>
                                <div class="form-row">
                                          <div class="form-group col-md-4">
                                            <label for="id_inventaris">Id Inventaris</label>
                                            <select name="id_inventaris" id="id_inventaris" class="form-control mt-1">
                                              @foreach($databarang as $d)
                                                <option value = "{{ $d->id }}"> - {{ $d->kode_inventaris}} - {{$d->nama}}</option>
                                              @endforeach
                                            </select>
                                            </div>
                                </div> 
                                <div class="form-row">
                                          <div class="form-group col-md-4">
                                            <label for="jumlah">Jumlah Barang</label>
                                            <input name="jumlah" type="text" class="form-control" id="jumlah">
                                          </div>
                                </div> 
                                <div class="form-row">
                                          <div class="form-group col-md-4">
                                            <label for="tanggal_kembali">Tanggal Kembali</label>
                                            <input name="tanggal_kembali" type="date" class="form-control" id="tanggal_kembali">
                                          </div>
                                </div>
                </div>
            <p></p>
                <button class="btn btn-success">Confirm</button>
                <button type="submit" class="btn btn-danger"><a href="{{route('returnop')}}" style="color:white;">Cancel</a></button>
            
</body>

     <!--   Core JS Files   -->
     <link href="{{ asset('/css/bootstrap.min.css') }}" rel="stylesheet" />
    <script src="{{ asset('/js/jquery.3.2.1.min.js') }}" type="text/javascript"></script>
	<script src="{{ asset('/js/bootstrap.min.js') }}" type="text/javascript"></script>

	<!--  Charts Plugin -->
	<script src="{{ asset('/js/chartist.min.js') }}"></script>

    <!--  Notifications Plugin    -->
    <script src="{{ asset('/js/bootstrap-notify.js') }}"></script>

    <!-- Light Bootstrap Table Core javascript and methods for Demo purpose -->
	<script src="{{ asset('/js/light-bootstrap-dashboard.js?v=1.4.0') }}"></script>

	<!-- Light Bootstrap Table DEMO methods, don't include it in your project! -->
	<script src="{{ asset('/js/demo.js') ]}"></script>

	<script type="text/javascript">
    	$(document).ready(function(){

        	demo.initChartist();

        	// $.notify({
         //    	icon: 'pe-7s-hand',
         //    	message: "Welcome to <b>Invenity App</b> - an easy vibes for borrowing the inventory."

         //    }
         ({
                type: 'info',
                timer: 4000
            });

    	});
	</script>

</html>
