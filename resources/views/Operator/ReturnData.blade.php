<!doctype html>
<html lang="en">
<head>

	<meta charset="utf-8" />
	<link rel="icon" type="image/png" href="assets/img/favicon.ico">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

	<title>invenity.org</title>

	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />

    <link href="{{ asset('/css/bootstrap.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('/css/animate.min.css') }}" rel="stylesheet"/>
    <link href="{{ asset('/css/bootstrap.css?v=1.4.0') }}" rel="stylesheet"/>
    <link href="{{ asset('/css/demo.css') }}" rel="stylesheet" />
    <!--     Fonts and icons     -->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300' rel='stylesheet' type='text/css'>
    <link href="{{ asset('/css/pe-icon-7-stroke.css') }}" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/dataTables.css') }}">

</head>
<body>
<div class="wrapper" style="margin-left:40vh;">
    <div class="sidebar">

    	<div class="sidebar-wrapper">
            <div class="logo">
                <img src="{{asset('/img/profile.png') }}" style="width: 15vh; height: 15vh; margin-left: 8vh;">
                <h6 class="text-center">{{ auth()->user()->nama }}</h6>
            </div>
            <ul class="nav">
            <li class="active">
                    <a href="{{route('dashboardop')}}">
                        <i class="pe-7s-graph"></i>
                        <p>Home</p>
                    </a>
                </li>
                <li>
                    <a href="{{route('pegawaiop')}}">
                        <i class="pe-7s-note2"></i>
                        <p>User Management</p>
                    </a>
                </li>
                <li>
                    <a href="{{route('barangop')}}">
                        <i class="pe-7s-news-paper"></i>
                        <p>Inventory Management</p>
                    </a>
                </li>
                <li>
                    <a href="{{route('loanop')}}">
                        <i class="pe-7s-note"></i>
                        <p>Loan Data</p>
                    </a>
                </li>
                <li>
                    <a href="{{route('returnop')}}">
                        <i class="pe-7s-refresh"></i>
                        <p>Return Data</p>
                    </a>
                </li>
                <li>
                    <a href="{{route('settingop')}}">
                        <i class="pe-7s-settings"></i>
                        <p>Account Setting</p>
                    </a>
                </li>
				<li class="active-pro">
                    <a href="{{route('logout')}}">
                        <i class="pe-7s-rocket"></i>
                        <p>Log Out</p>
                    </a>
                </li>
            </ul>
    	</div>
    </div>


        <div class="container" style="margin-top:5%;">
            <div class="row">
                 <div class="card col-sm-4" style="width: 20rem; margin-top: 2vh; margin-left:2vh;">
                         <div class="card-body">
                           <h3 class="card-title" style="text-align:center; color: darkgreen">200</h3>
                           <h6 class="card-subtitle mb-2 text-muted" style="text-align:center; margin-bottom: 2vh;">Inventory</h6>
                          
                         </div>
                 </div>
                 <div class="card col-sm-4" style="width: 20rem; margin-top: 2vh; margin-left:2vh;">
                         <div class="card-body">
                           <h3 class="card-title" style="text-align:center; color: red">2</h3>
                           <h6 class="card-subtitle mb-2 text-muted" style="text-align:center; margin-bottom: 2vh;">Operator</h6>
                          
                         </div>
                 </div>
                 <div class="card col-sm-4" style="width: 20rem; margin-top: 2vh; margin-left:2vh;">
                         <div class="card-body">
                           <h3 class="card-title" style="text-align:center; color:rgb(53, 50, 205)">50</h3>
                           <h6 class="card-subtitle mb-2 text-muted" style="text-align:center; margin-bottom: 2vh;">Borrowwer</h6>
                          
                         </div>
                 </div>
         </div>
         
     </div>
                      <div class="container">
                            <h2>Return Data</h2>           
                            <table class="table table-hover" id="myTable">
                              <thead>
                                <tr>
                                <th>ID PENGEMBALIAN</th>
                                <th>ID PEMINJAMAN</th>
                                <th>NAMA PEGAWAI</th>
                                <th>ACTION</th>
                                
                                </tr>
                              </thead>
                              <tbody>
                                <tr>
                                @foreach($datakembali as $p)
                                  <td>{{ $p-> id}}</td>
                                  <td>{{ $p-> id_peminjaman }}</td>
                                  <td>{{ $p-> nama_pegawai }}</td>
                                  <td>
                                    <a href="{{ route('detailreturn',$p->id) }}"><i class="pe-7s-info">
                                    </i></a>
                                  </td>
                                </tr>
                                @endforeach
                                
                              </tbody>
                            </table>
                          </div>
        </form>               
        <p></p>   
        <button type="button" class="btn btn-primary" style="margin-left:1em;">
          <a href="{{ route('addkembaliop') }}" style="color:white;">Insert Data</a>
        </button>
                        <footer class="footer">

                            <p class="copyright pull-right">
                            &copy; <script>document.write(new Date().getFullYear())</script> <a href="http://www.creative-tim.com">Invenity Tim</a>, made with love for an easy vibes.
                            </p>
                            </div>
                        </footer>
              
</body>

<link href="{{ asset('/css/bootstrap.min.css') }}" rel="stylesheet" />
    <script type="text/javascript" charset="utf8" src="{{ asset('assets/js/jquery.min.js') }}"></script>
    <script src="{{ asset('/js/bootstrap.min.js') }}" type="text/javascript"></script>

	<!--  Charts Plugin -->
	<script src="{{ asset('/js/chartist.min.js') }}"></script>

    <!--  Notifications Plugin    -->
    <script src="{{ asset('/js/bootstrap-notify.js') }}"></script>

    <!-- Light Bootstrap Table Core javascript and methods for Demo purpose -->
	<script src="{{ asset('/js/light-bootstrap-dashboard.js?v=1.4.0') }}"></script>

	<!-- Light Bootstrap Table DEMO methods, don't include it in your project! -->
	<script src="{{ asset('js/demo.js') }}"></script>
    <script type="text/javascript" charset="utf8" src="{{ asset('assets/js/dataTables.js') }}"></script>
    <script type="text/javascript" charset="utf8" src="{{ asset('assets/js/jquery.dataTables.min.js') }}"></script>

	<script type="text/javascript">
    	$(document).ready( function () {
            $('#myTable').DataTable();
    } );
	</script>
</html>
